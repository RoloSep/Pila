package cl.ubb.Pila;


import static org.junit.Assert.assertEquals;

import org.junit.Test;



public class PilaTest {
	
	String n = null;
	String m = null;

	@Test
	public void InicializarPilaVacia() {
		/*arrange*/
		Pila p = new Pila();
		String resultado;
		/*act*/
		resultado = p.apilar(n,m);
		/*assert*/
		assertEquals(resultado,null);

}
	@Test
	public void IngresarNumeroUnoALaPilaYNoVacia() {
		/*arrange*/
		Pila p = new Pila();
		String resultado;
		/*act*/
		resultado = p.apilar("1",m);
		/*assert*/
		assertEquals(resultado,"NoVacia");

}
	@Test
	public void IngresarNumero1y2ALaPilaYNoVacia() {
		/*arrange*/
		Pila p = new Pila();
		String resultado;
		/*act*/
		resultado = p.apilar("1","2");
		/*assert*/
		assertEquals(resultado,"NoVacia");

}
	@Test
	public void IngresarNumero1y2ALaPilaYTama�oEs2() {
		/*arrange*/
		Pila p = new Pila();
		String resultado;
		String tama�o = null;
		/*act*/
		tama�o = p.tama�o("1","2");
		/*assert*/
		assertEquals(tama�o,"2");

}
	
	


}
